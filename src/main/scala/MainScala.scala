import java.io.{BufferedWriter, File, FileWriter}

import scala.collection.mutable

/**
  * Created by fabio on 23-02-2017.
  */
class ScalaWorld {
  override def toString: String = "ScalaWorld"
}
case class Video(id:Int, size:Int)
case class Cache(id:Int, var men:Int, var vID: Seq[Int], var conetions:Seq[Endpoint]) {
  def addVideo(v:Video):Boolean = {
    if(men + v.size < MainScala.info.cacheSize){
      this.men += v.size
      this vID = vID :+ v.id
      true
    } else {false}
  }
  override def toString: String = id.toString + " " + vID.mkString(" ")
}
case class Endpoint(id:Int,distance:Int,nCache:Int,cacheData:Seq[(Int,Int)])
case class Request(vID:Int, eID:Int, req:Int)
case class Info(v:Int,e:Int,r:Int,nCaches:Int,cacheSize:Int)

case class Data(var videos: Seq[Video],endpoints: Seq[Endpoint],requests: Seq[Request], var caches:mutable.Map[Int,Cache])


object MainScala {
  //def readfile = io.Source.fromFile("input").getLines().toStream

  var info: Info = _


  def main(args: Array[String]): Unit = {
    // val name = "input"
    //val name = "kittens"
    //val name = "me_at_the_zoo"
    val name = "trending_today"
    //val name = "videos_worth_spreading"

    val readfile = io.Source.fromResource(name + ".in").getLines()

    //readfile.foreach(e => println(e))
    val infoLine = readfile.next().split(' ').map(_.toInt)
    info = Info(infoLine(0),infoLine(1),infoLine(2),infoLine(3),infoLine(4))
    val vLine = readfile.next().split(' ').map(_.toInt)

    val caches = mutable.Map[Int,Cache]()
    for (i <- 0 to info.nCaches - 1) yield {
      caches(i) = Cache(i,0,Seq[Int](),Seq[Endpoint]())
      //caches.+((i,Cache(i,0,Seq[Int](),Seq[Int]())))
    }

    val endpoints = for (eID <- 0 to info.e - 1) yield {
      val eLine = readfile.next().split(' ').map(_.toInt)
      val distance = eLine(0)
      val endpointSeq = for (i <- 0 to eLine(1) - 1) yield {
        val auxLine = readfile.next().split(' ').map(_.toInt)
        (auxLine(0), auxLine(1)) // id lat
      }

      Endpoint(eID, distance, eLine(1), endpointSeq
        .map{case(a,b)=>(a,distance-b)}
        .sortBy{ case (a:Int,b:Int) => b}
      )

    }
    endpoints.foreach( e =>
      e.cacheData.foreach{ case (idCache, distants) =>
        val c = caches(idCache)
        caches(idCache)=c.copy(conetions = (c.conetions :+ (e)))
      }
    )



    val requests = for(i <- 0 to info.r - 1) yield {
      val rLine = readfile.next().split(' ').map(_.toInt)
      Request(rLine(0),rLine(1),rLine(2))
    }




    val data = Data(vLine.zipWithIndex.map{ case (size,id) => Video(id,size)}, endpoints,requests,caches)
    data.caches = data.caches.filter{ case (i:Int,c:Cache) => ! c.conetions.isEmpty }
    //data.videos = data.videos.sortBy(_.size)//.filter{ (e:Video) => e.size < info.cacheSize}

    val max = data.videos.map(v => v.size).max
    val min = data.videos.map(v => v.size).min

    var aaa = requests.flatMap{ r =>
      val e = endpoints(r.eID)
      val v = data.videos(r.vID)
      val aux = 1.01 - ((v.size - min)/(max - min))

      val calculo = e.cacheData.map{case (cacheID, lat) =>
        (r.req * lat * aux,r, cacheID)
      }
      calculo
    }
    aaa= aaa.sortBy(_._1).reverse
    println(aaa)

    /*val cachesSorted = data.caches.map(_._2).toSeq.sortBy{c =>
      val lat = c.conetions.map(e =>

      )
    }*/

    var index = 0
    aaa.foreach{ case(_,Request(vID,eID,_),cacheID) =>
      //println(vID)
     data.caches(cacheID).addVideo(data.videos(vID))
    }
    println(data.caches)
    /*data.videos.foreach(v => {
      caches(index % caches.size).addVideo(v)
      index += 1
    })*/

    //println(info)
    //println(endpoints)
    //println(data)

    println(data.caches.filter{ case (i:Int,c:Cache) => (c.men != 0) } .size)
    data.caches.map(_._2).foreach(println)


    val file = new File(name + ".out")
    val bw = new BufferedWriter(new FileWriter(file))
   bw.write(data.caches.filter{ case (i:Int,c:Cache) => (c.men != 0) } .size + "\n")
   data.caches.map(_._2).foreach(e => bw.write(e.toString + "\n"))
   bw.close()
  }
}
